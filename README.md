#  Uživatelské rozhraní pro portál FIKS 

[is.fit.cvut.cz](https://is.fit.cvut.cz/group/intranet/zp/mytasks)

## Jazyk ##
český

## Popis (česky) (max. 1000 znaků) ##

Cílem práce je provést analýzu současného uživatelského rozhraní webového portálu FIKS (Fiťácký informatický korespondenční seminář) a na jejím základě navrhnout nové uživatelské rozhraní. Portál by měl být přehledný a umožňovat snadné ovládání nejen na stolním počítači, ale také na mobilním telefonu a tabletu.

1. Proveďte analýzu stávající funkcionality portálu FIKS.
2. Proveďte analýzu stávajícího uživatelského rozhraní portálu FIKS.
3. Na základě poznatků z analýzy navrhněte nové uživatelské rozhraní, případně zlepšete některé stávající prvky.
	1. Navrhněte drátěný model portálu.
	2. Navrhněte grafickou podobu portálu.
4. Návrh uživatelského rozhraní implementujte do podoby klikatelného prototypu.
5. Otestujte uživatelské rozhraní prototypu.
Seznam odborné literatury (max. 1000 znaků)
Dodá vedoucí práce.
