\select@language {czech}
\select@language {czech}
\contentsline {subsection}{Odkaz na tuto pr{\' a}ci}{viii}{section*.1}
\contentsline {chapter}{{\' U}vod}{1}{chapter*.5}
\contentsline {section}{C\IeC {\'\i }l bakal\IeC {\'a}\IeC {\v r}sk\IeC {\'e} pr\IeC {\'a}ce}{2}{section*.6}
\contentsline {chapter}{\chapternumberline {1}Teoretick\IeC {\'y} rozbor}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Koresponden\IeC {\v c}n\IeC {\'\i } semin\IeC {\'a}\IeC {\v r}}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Human-computer interaction}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}U\IeC {\v z}ivatelsk\IeC {\'e} rozhran\IeC {\'\i }}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}U\IeC {\v z}ivatelsk\IeC {\'y} pro\IeC {\v z}itek}{4}{section.1.4}
\contentsline {section}{\numberline {1.5}Pou\IeC {\v z}itelnost webu}{4}{section.1.5}
\contentsline {section}{\numberline {1.6}P\IeC {\v r}\IeC {\'\i }stupnost webu}{5}{section.1.6}
\contentsline {section}{\numberline {1.7}Responsivn\IeC {\'\i } webdesign}{5}{section.1.7}
\contentsline {subsection}{\numberline {1.7.1}P\IeC {\v r}\IeC {\'\i }stupy k n\IeC {\'a}vrhu}{7}{subsection.1.7.1}
\contentsline {subsubsection}{\numberline {1.7.1.1}Desktop-first}{7}{subsubsection.1.7.1.1}
\contentsline {subsubsection}{\numberline {1.7.1.2}Mobile-first}{7}{subsubsection.1.7.1.2}
\contentsline {chapter}{\chapternumberline {2}Postupy a metody p\IeC {\v r}i tvorb\IeC {\v e} webu}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Objevov\IeC {\'a}n\IeC {\'\i }}{9}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Anal\IeC {\'y}za p\IeC {\v r}\IeC {\'\i }buzn\IeC {\'y}ch a konkuren\IeC {\v c}n\IeC {\'\i }ch web\IeC {\r u}}{9}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Anal\IeC {\'y}za sou\IeC {\v c}asn\IeC {\'e}ho stavu}{10}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}SWOT anal\IeC {\'y}za}{10}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}U\IeC {\v z}ivatelsk\IeC {\'y} v\IeC {\'y}zkum}{10}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Persony}{10}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Rozhovory, dotazn\IeC {\'\i }ky}{10}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Po\IeC {\v r}\IeC {\'a}d\IeC {\'a}n\IeC {\'\i } karet}{10}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}N\IeC {\'a}vrh webu}{11}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Obsah}{11}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Dr\IeC {\'a}t\IeC {\v e}n\IeC {\'y} model}{11}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Grafick\IeC {\'a} \IeC {\v s}ablona}{11}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Testov\IeC {\'a}n\IeC {\'\i }}{11}{section.2.4}
\contentsline {chapter}{\chapternumberline {3}Anal\IeC {\'y}za}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Anal\IeC {\'y}za konkurence}{13}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}morous.fel.cvut.cz}{13}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}ksi.fi.muni.cz}{13}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}ksp.mff.cuni.cz}{13}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Sou\IeC {\v c}asn\IeC {\'e} \IeC {\v r}e\IeC {\v s}en\IeC {\'\i }}{13}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Funkcionalita}{13}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}U\IeC {\v z}ivatelsk\IeC {\'e} rozhran\IeC {\'\i }}{13}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Persony}{13}{subsection.3.2.3}
\contentsline {chapter}{\chapternumberline {4}N\IeC {\'a}vrh}{15}{chapter.4}
\contentsline {section}{\numberline {4.1}Menu}{15}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Hamburger menu}{15}{subsection.4.1.1}
\contentsline {section}{\numberline {4.2}Formul\IeC {\'a}\IeC {\v r}e pro registraci}{15}{section.4.2}
\contentsline {section}{\numberline {4.3}Captcha}{15}{section.4.3}
\contentsline {section}{\numberline {4.4}Typografie a sazba webu}{15}{section.4.4}
\contentsline {section}{\numberline {4.5}Diskusn\IeC {\'\i } f\IeC {\'o}rum}{15}{section.4.5}
\contentsline {section}{\numberline {4.6}Chybov\IeC {\'e} str\IeC {\'a}nky}{15}{section.4.6}
\contentsline {chapter}{\chapternumberline {5}Testov\IeC {\'a}n\IeC {\'\i }}{17}{chapter.5}
\contentsline {section}{\numberline {5.1}Pr\IeC {\r u}b\IeC {\v e}h testov\IeC {\'a}n\IeC {\'\i }}{17}{section.5.1}
\contentsline {section}{\numberline {5.2}Z\IeC {\'a}v\IeC {\v e}r testov\IeC {\'a}n\IeC {\'\i }}{17}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Prototyp}{17}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Sc\IeC {\'e}n\IeC {\'a}\IeC {\v r}e}{17}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}V\IeC {\'y}sledky testov\IeC {\'a}n\IeC {\'\i }}{17}{subsection.5.2.3}
\contentsline {chapter}{Z{\' a}v{\v e}r}{19}{chapter*.7}
\contentsline {chapter}{Literatura}{21}{section*.9}
\contentsline {appendix}{\chapternumberline {A}Seznam pou\IeC {\v z}it\IeC {\'y}ch zkratek}{23}{appendix.A}
\contentsline {appendix}{\chapternumberline {B}Obsah p\IeC {\v r}ilo\IeC {\v z}en\IeC {\'e}ho CD}{25}{appendix.B}
\contentsline {paragraph}{Ahoj}{25}{section*.10}
