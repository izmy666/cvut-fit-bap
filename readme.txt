Uživatelské rozhraní pro portál FIKS
=====

Obsah CD
########

Adresář /graphics/01-desktop obsahuje grafické šablony pro stolní počítače
Adresář /graphics/02-tablet obsahuje grafické šablony pro tablety
Adresář /graphics/01-mobile obsahuje grafické šablony pro mobilní zařízení

Adresář /prototype/01-desktop obsahuje klikatelný prototyp pro stolní počítače
Adresář /prototype/02-tablet obsahuje klikatelný prototyp pro tablety
Adresář /prototype/01-mobile obsahuje klikatelný prototyp pro mobilní zařízení

Adresář /wireframes obsahuje snímky wireframů (drátěných modelů)

Adresář /src/thesis obsahuje zdrojovou formu práce ve formátu LaTeX

Adresář /text/thesis.pdf obsahuje text práce ve formátu PDF